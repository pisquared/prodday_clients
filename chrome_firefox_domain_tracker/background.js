var SESSIONS = [];
var CURRENT_URL = undefined;
var PREVIOUS_DOMAIN = undefined;
var IS_ACTIVE = false;

function getNow() {
  return moment().utc().format("YYYY-MM-DD HH:mm:ss")
}

function sendUpdate() {
  if (VERBOSITY >= 2)
    console.log(SESSIONS);
  $.ajax({
    url: TRACKER_URL,
    method: 'post',
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({
      'tracker_id': DOMAIN_TRACKER_ID,
      'api_key': DOMAIN_API_KEY,
      'sessions': SESSIONS
    }),
    success: function (response) {
      if (VERBOSITY >= 2) {
        console.log('response:');
        console.log(response)
      }
    }
  });
}


function updateDomainStats(url) {
  var now = getNow();
  var domain = extractHostname(url);
  if (domain !== PREVIOUS_DOMAIN) {
    // new session, close previous
    if (SESSIONS.length !== 0)
      SESSIONS[SESSIONS.length - 1]['finished_ts'] = now;
    SESSIONS.push({
      'title': domain,
      'total_time': 0,
      'active_time': 0,
      'started_ts': now,
      'finished_ts': null
    })
  }
  SESSIONS[SESSIONS.length - 1]['total_time'] += 1;
  if (IS_ACTIVE) {
    SESSIONS[SESSIONS.length - 1]['active_time'] += 1;
    IS_ACTIVE = false;
  }
  // update the dict
  PREVIOUS_DOMAIN = domain;
  if (VERBOSITY >= 2)
    console.log(SESSIONS[SESSIONS.length - 1]);
}

function sendMessageToActiveTab(tabs) {
  var activeTab = tabs[0];
  chrome.tabs.sendMessage(activeTab.id, {
    "message": "change_tab"
  });
}

// --- CHROME ----
chrome.tabs.onUpdated.addListener(function (tab) {
  // Send a message to the active tab
  chrome.tabs.query({active: true, currentWindow: true}, sendMessageToActiveTab);
});

chrome.tabs.onActivated.addListener(function (tab) {
  // Send a message to the active tab
  chrome.tabs.query({active: true, currentWindow: true}, sendMessageToActiveTab);
});

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    if (request.message === "info_from_tab") {
      CURRENT_URL = request.url;
    }
  }
);

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    if (request.message === "activity_detected" && !IS_ACTIVE) {
      IS_ACTIVE = true;
    }
  }
);

if (TRACK_DOMAINS) {
  if (VERBOSITY >= 1)
      console.log("Starting domain tracker. Config: VERBOSITY: " + VERBOSITY + " TRACKER_URL: " + TRACKER_URL + " TRACK_LOCATION: " + TRACK_LOCATION + ", TRACK_DOMAINS: " + TRACK_DOMAINS + ", FLUSH_SECONDS: " +FLUSH_SECONDS);
  setInterval(function () {
    if (CURRENT_URL) {
      updateDomainStats(CURRENT_URL);
    }
  }, 1000);

  setInterval(function () {
    if (VERBOSITY >= 1)
      console.log('sending update: ' + SESSIONS.length + ' elements');
    SESSIONS[SESSIONS.length - 1]['finished_ts'] = getNow();
    sendUpdate();
    SESSIONS = [];
    PREVIOUS_DOMAIN = undefined;
    IS_ACTIVE = false;
  }, FLUSH_SECONDS * 1000);

}
