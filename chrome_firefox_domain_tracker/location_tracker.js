var START_TIME = getNow();

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(injectLocation, locationError);
  } else {
    console.log("NOT_SUPPORTED");
  }
}

function injectLocation(position) {
  if (DEBUG)
    console.log(
      '= LOC: lat: ' + position.coords.latitude + '\n' +
      '       lon: ' + position.coords.longitude + '\n' +
      '       acc: ' + position.coords.accuracy
    );
  $.ajax({
    url: TRACKER_URL,
    method: 'post',
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({
      'tracker_id': LOCATION_TRACKER_ID,
      'api_key': LOCATION_API_KEY,
      'sessions': [{
        'title': position.coords.latitude + ',' + position.coords.longitude,
        'started_ts': START_TIME,
        'finished_ts': getNow(),
        'latitude': position.coords.latitude,
        'longitude': position.coords.longitude,
        'location_accuracy': position.coords.accuracy
      }]

    })
  }).done(function (r) {
    if (DEBUG)
      console.log('= SENT Updated location from js');
  }).fail(function (jqXHR) {
    var message = "<strong>Something went wrong.</strong>";
    if (jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.message)
      message += " " + jqXHR.responseJSON.message;
    if (DEBUG && jqXHR.status != 500) {
      var stacktrace = "<p style='font-family: Courier, monospace'>";
      stacktrace += "<br>DEBUG:<br>------<br>" + jqXHR.responseText;
      stacktrace += "</p>";
      message += stacktrace;
    }
    $('#error-messages').addClass('alert-danger').html(message).show();
  });
  START_TIME = getNow();
}

function locationError(error) {
  switch (error.code) {
    case error.PERMISSION_DENIED:
      console.log("PERMISSION_DENIED");
      break;
    case error.POSITION_UNAVAILABLE:
      console.log("POSITION_UNAVAILABLE");
      break;
    case error.TIMEOUT:
      console.log("TIMEOUT");
      break;
    case error.UNKNOWN_ERROR:
      console.log("UNKNOWN_ERROR");
      break;
  }
}

if (TRACK_LOCATION)
  getLocation();
