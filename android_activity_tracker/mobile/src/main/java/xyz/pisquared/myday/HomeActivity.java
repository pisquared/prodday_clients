package xyz.pisquared.myday;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.auth_preferences_file_key), Context.MODE_PRIVATE);
        final String auth_token = sharedPref.getString(getString(R.string.auth_token), "");
        Intent intent;
        // TODO[pisquared]: this should be auth_token.equals to
        if (!auth_token.equals("")) {
            // no auth, start login activity
            intent = new Intent(this, LoginActivity.class);

        } else {
            // there is already auth credentials, start main activity
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
    }
}
