import os

import datetime
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_

app = Flask(__name__)
app.config['SECRET_KEY'] = '1235712356fhdksahfsdjadsfhjkads'
sqllite_path = os.path.join(os.getcwd(), 'history.db')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(sqllite_path)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class HistoryCommand(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    host = db.Column(db.Unicode)
    created_dt = db.Column(db.DateTime)
    command = db.Column(db.Unicode)

    @staticmethod
    def ts_to_dt(ts):
        return datetime.datetime.fromtimestamp(int(ts))

    @property
    def created_ts(self):
        return self.created_dt.strftime("%s")

    def serialize(self):
        return {
            'id': self.id,
            'command': self.command,
            'created_dt': str(self.created_dt),
            'created_ts': self.created_ts,
            'host': self.host,
        }

    def __repr__(self):
        return '<HistoryCommand (%r): %r>' % (self.created_dt, self.command)


@app.route('/new_batch', methods=['POST'])
def route_new_batch_command():
    history_commands_json = request.get_json()
    hcs = []
    for history_command_json in history_commands_json:
        ts = history_command_json.get('created_ts')
        created_dt = HistoryCommand.ts_to_dt(ts)
        command = history_command_json.get('command')
        host = history_command_json.get('host')
        history_command = HistoryCommand(command=command,
                                         created_dt=created_dt,
                                         host=host)
        hcs.append(history_command)
        db.session.add(history_command)
    db.session.commit()
    return jsonify([hc.serialize() for hc in hcs])


@app.route('/new', methods=['POST'])
def route_new_command():
    ts = request.form.get('ts')
    created_dt = HistoryCommand.ts_to_dt(ts)
    command = request.form.get('command')
    host = request.form.get('host')
    history_command = HistoryCommand(command=command,
                                     created_dt=created_dt,
                                     host=host)
    db.session.add(history_command)
    db.session.commit()
    return jsonify(history_command.serialize())


@app.route('/get')
def route_get():
    since = request.args.get('since')
    host = request.args.get('host')
    host_not = request.args.get('host_not')
    if since and host_not:
        since_dt = HistoryCommand.ts_to_dt(since)
        commands = HistoryCommand.query.filter(and_(HistoryCommand.created_dt > since_dt,
                                                    HistoryCommand.host != host_not)).all()
    elif host:
        if since:
            since_dt = HistoryCommand.ts_to_dt(since)
            commands = HistoryCommand.query.filter(and_(HistoryCommand.created_dt > since_dt,
                                                        HistoryCommand.host == host)).all()
        else:
            commands = HistoryCommand.query.filter(and_(HistoryCommand.host == host)).all()
    else:
        commands = HistoryCommand.query.all()
    return jsonify([command.serialize() for command in commands])


if __name__ == '__main__':
    if not os.path.exists(sqllite_path):
        db.create_all()
    # app.run(host='0.0.0.0', port=80)
    app.run(debug=True)
