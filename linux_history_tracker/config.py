import os
import socket

TRACKER_SESSIONS_API_URL = "http://localhost:5000/api/trackers/{}/tracker-events"
TRACKER_URL = "http://localhost:5000/tracker-events/create/batch"

FETCH_SLEEP = 10
BASH_HISTORY_FILE = os.path.join(os.environ.get('HOME'), '.bash_eternal_history')
HOST = os.environ.get('HISTORY_HOSTNAME') or socket.gethostname()