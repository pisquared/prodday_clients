#!/usr/bin/env python
"""
* Requires xdotool to be installed
"""
import os
import subprocess

import subprocess as sp
import threading
import time
from pprint import pprint

import psutil
import requests
from datetime import datetime
from pynput import mouse, keyboard

from config import TRACKER_URL, MONITOR_INTERVAL, FLUSH_SECONDS, VERBOSITY as DEFAULT_VERBOSITY
from sensitive import API_KEY, TRACKER_ID

VERBOSITY = os.environ.get('LINUX_APP_TRACKER_VERBOSITY', DEFAULT_VERBOSITY)
if VERBOSITY.isdigit():
    VERBOSITY = int(VERBOSITY)

print(VERBOSITY)


def get_now():
    return datetime.utcnow()


class Monitor(object):
    def __init__(self, tracker_id, time_bucket_size=60):
        self.tracker_id = tracker_id
        self.time_bucket_size = time_bucket_size

        self.sessions = []
        self.previous_process_name = ''
        self.active = False
        self.ml, self.kl = None, None

    def set_active(self, *args, **kwargs):
        self.active = True

    def monitor_mouse(self):
        self.ml = mouse.Listener(
            on_move=self.set_active,
            on_click=self.set_active,
            on_scroll=self.set_active)
        self.ml.start()

    def monitor_keyboard(self):
        self.kl = keyboard.Listener(
            on_press=self.set_active,
            on_release=self.set_active)

    def _get_current_process_name(self):
        try:
            output = sp.check_output(["xdotool", "getactivewindow", "getwindowpid"])
        except sp.CalledProcessError as e:
            if VERBOSITY >= 1:
                print("ERROR: Problem checking output of xdotool: {}".format(e))
            return False
        try:
            decoded_output = output.decode("utf-8")
        except Exception as e:
            if VERBOSITY >= 1:
                print("ERROR: Problem decoding output of xdotool: {}".format(e))
            return False
        if type(decoded_output) not in [str, unicode]:
            if VERBOSITY >= 1:
                print("ERROR: Invalid output of xdotool: {}".format(decoded_output))
            return False
        decoded_output = decoded_output.strip()
        if not decoded_output.isdigit():
            if VERBOSITY >= 1:
                print("ERROR: Invalid output of xdotool (not a digit): {}".format(decoded_output))
            return False
        pid = int(decoded_output)
        process_name = psutil.Process(pid).name()
        return process_name

    def monitor_active_window(self):
        threading.Thread(target=self.monitor_mouse).start()
        threading.Thread(target=self.monitor_keyboard).start()
        for _ in range(FLUSH_SECONDS):
            now = get_now()
            process_name = self._get_current_process_name()
            if not process_name:
                continue
            if not process_name == self.previous_process_name:
                # new session for a new process
                # close previous session
                if len(self.sessions):
                    self.sessions[-1]['finished_ts'] = str(now)
                self.sessions.append({
                    'title': process_name.decode("utf-8"),
                    'total_time': 0,
                    'active_time': 0,
                    'started_ts': str(now),
                    'finished_ts': None
                })
            self.sessions[-1]['total_time'] += MONITOR_INTERVAL
            if self.active:
                self.sessions[-1]['active_time'] += MONITOR_INTERVAL
                self.active = False
            self.previous_process_name = process_name
            if VERBOSITY >= 2:
                pprint(self.sessions[-1])
            time.sleep(MONITOR_INTERVAL)

        self.ml.stop()
        self.kl.stop()
        now = get_now()
        self.sessions[-1]['finished_ts'] = str(now)
        if VERBOSITY >= 2:
            print('end monitoring loop')
        self.push_update()

    def push_update(self):
        payload = {
            'tracker_id': self.tracker_id,
            'sessions': self.sessions,
            'api_key': API_KEY,
            'tz_offset_seconds': -time.timezone,
        }
        if VERBOSITY >= 1:
            print("Pushing payload length: {}".format(len(payload)))
        if VERBOSITY >= 2:
            pprint(payload)
        try:
            response = requests.post(TRACKER_URL, json=payload, verify=False)
            if VERBOSITY >= 2:
                print(response.content)
                print(response.status_code)
                print('Pushed update, restarting bucket.')
        except Exception as e:
            if VERBOSITY >= 1:
                print('ERROR: Failed to post update!')
                print(e)


def loop():
    while True:
        monitor = Monitor(tracker_id=TRACKER_ID, time_bucket_size=FLUSH_SECONDS)
        monitor.monitor_active_window()


if __name__ == '__main__':
    print(
        "Starting monitoring loop. VERBOSITY: \"{}\", TRACKER_URL: \"{}\", MONITOR_INTERVAL: \"{}\", FLUSH_SECONDS: \"{}\"".format(
            VERBOSITY, TRACKER_URL, MONITOR_INTERVAL, FLUSH_SECONDS
        ))
    loop()
