#!/bin/bash
set -e
set -x
sed "s|PWD|`pwd`|" linux_app_tracker.service.sample > linux_app_tracker.service
sudo cp linux_app_tracker.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable linux_app_tracker.service
sudo systemctl start linux_app_tracker.service
