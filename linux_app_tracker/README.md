# Linux (Ubuntu) App tracker

This program tracks the current used process title and posts in 15 second intervals to prodday.com

## Install

```
# install dependencies
sudo apt install xdotool virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
cp sensitive.py.sample sensitive.py

# enable the service
sudo cp linux_app_tracker.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable linux_app_tracker
sudo systemctl start linux_app_tracker
```
