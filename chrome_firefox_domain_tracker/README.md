# Browser Addon domain Tracker

This program tracks the current domain and posts in 15 second intervals to prodday.com

## Install on Firefox
Go to [about:debugging#addons](about:debugging#addons), click on `Load Temporary Add-On` and select the manifest file.
You can click on `Debug` to see logging.

## Install on Chrome
Go to [chrome://extensions](chrome://extensions/), check `Developer mode` and click on Load unpacked extension.
You can click on `background page` to see logging.