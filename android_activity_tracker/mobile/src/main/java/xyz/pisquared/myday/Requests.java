package xyz.pisquared.myday;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by pi2 on 07/05/17.
 */

class Requests {
    private static final String TAG = "Requests";
    private Context mContext;

    Requests(Context context) {
        mContext = context;
    }


    private void storeAuthenticationCredentials(String authToken) {
        Log.i(TAG, authToken);
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                mContext.getResources().getString(R.string.auth_preferences_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(mContext.getString(R.string.auth_token), authToken);
        editor.apply();
    }

    void authenticate(String username, String password) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = Constants.AUTH_URL;

        Map<String, String> credentials = new HashMap<>();
        credentials.put("email", username);
        credentials.put("password", password);

        final JSONObject payload = new JSONObject(credentials);

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                payload,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "Response is: " + response.toString());
                        try {
                            String authToken = response
                                    .getJSONObject("response")
                                    .getJSONObject("user")
                                    .getString("authentication_token");
                            storeAuthenticationCredentials(authToken);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, "That didn't work! Request was: " + payload.toString());
                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }
}
