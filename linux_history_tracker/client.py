#!/usr/bin/env python
import os
import threading
import time
import traceback

import datetime
import requests
from dateutil import parser as dt_parser
from sqlalchemy import create_engine, Integer, Unicode, DateTime, Column, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

import sensitive
from config import TRACKER_URL, TRACKER_SESSIONS_API_URL, BASH_HISTORY_FILE, HOST, FETCH_SLEEP

Base = declarative_base()
sqllite_path = os.path.join(os.getcwd(), 'local_history.db')
engine = create_engine('sqlite:///{}'.format(sqllite_path), echo=False)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
poison_pill = None


def create_db():
    Base.metadata.create_all(engine)


def session_commit(session):
    try:
        session.commit()
    except Exception as e:
        print(e)
        session.rollback()
    finally:
        session.close()


class HistoryCommand(Base):
    __tablename__ = 'history_command'

    id = Column(Integer, primary_key=True)
    host = Column(Unicode)
    created_dt = Column(DateTime)
    command = Column(Unicode)
    synced = Column(Boolean, default=False)

    @staticmethod
    def ts_to_dt(ts):
        return datetime.datetime.fromtimestamp(int(ts))

    @property
    def created_ts(self):
        return self.created_dt.strftime("%s")

    def serialize(self):
        return {
            'key': self.command,
            'time': str(self.created_dt),
            'meta': {
                'id': self.id,
                'synced': self.synced,
                'host': self.host,
            }
        }

    def __repr__(self):
        return '<HistoryCommand (%r): %r>' % (self.created_dt, self.command)


def batch_post_request(history_commands):
    session = Session()
    for history_command in history_commands:
        session.add(history_command)
    print("  > Posting: {}".format(len(history_commands)))
    try:
        payload = dict(
            api_key=sensitive.API_KEY,
            tracker_id=sensitive.TRACKER_ID,
            events=[history_command.serialize() for history_command in history_commands]
        )
        response = requests.post(TRACKER_URL, json=payload)
        print("  > Success!")
        response.json()
        for history_command in history_commands:
            history_command.synced = True
            session.add(history_command)
    except Exception as e:
        print("  > FAIL!")
        print(e)
        traceback.print_exc()
    session_commit(session)


def post_request(history_command):
    session = Session()
    session.add(history_command)
    print("  > Posting: {}".format(history_command))
    try:
        payload = dict(
            api_key=sensitive.API_KEY,
            tracker_id=sensitive.TRACKER_ID,
            sessions=[history_command.serialize()]
        )
        response = requests.post(TRACKER_URL, json=payload)
        print("  > Success!")
        response.json()
        history_command.synced = True
    except:
        print("  > FAIL!")
        traceback.print_exc()
        history_command.synced = False
    session.add(history_command)
    session_commit(session)


def follow(fname):
    with open(BASH_HISTORY_FILE) as f:
        f.seek(0, 2)  # Go to the end of the file
        # HACK: in order not to hang on the last command
        yield_once = False
        while True:
            line = f.readline()
            if not line:
                if yield_once:
                    yield_once = False
                    yield
                time.sleep(0.1)  # Sleep briefly
                continue
            yield line
            print(">>> Listening for commands...")
            yield_once = True


def parse_history_file(iterable):
    create, ts, command = False, None, u""
    for line in iterable:
        print(">>> parsing {}".format(line))
        if line is None or (line.startswith('#1') and len(line) == 12):
            if not ts:
                ts = int(line[1:]) if line.startswith('#') else int(line)
            else:
                history_command = HistoryCommand(command=command,
                                                 created_dt=HistoryCommand.ts_to_dt(ts),
                                                 host=HOST)
                print(">>> Created {}".format(history_command))
                if line is None:
                    ts, command = 0, u""
                else:
                    ts, command = int(line[1:]) if line.startswith('#') else int(line), u""
                yield history_command
        else:
            command += line.decode('utf-8')


def get_local_all():
    with open(BASH_HISTORY_FILE) as f:
        local_commands = parse_history_file(iterable=f.readlines())
    return [c for c in local_commands]


def fetch_remote_all():
    remote_commands = []
    try:
        response = requests.get(TRACKER_SESSIONS_API_URL.format(sensitive.TRACKER_ID),
                                params={'api_key': sensitive.API_KEY, 'tracker_type': 'bash_history'})
    except:
        print('No connection to server!')
        return remote_commands
    response_json = response.json()
    tracker_events = response_json.get('tracker_events')
    for tracker_event in tracker_events:
        tracker_event_meta = tracker_event.get('meta', {})
        history_command = HistoryCommand(command=tracker_event['key'],
                                         created_dt=dt_parser.parse(tracker_event['time']),
                                         host=tracker_event_meta.get('client'),
                                         )
        remote_commands.append(history_command)
    return remote_commands


def create_commands_cache(commands):
    commands_cache = {}
    for command in commands:
        commands_cache[command.created_ts] = command
    return commands_cache


def merge_local_remote(local_commands, remote_commands):
    push_session = []
    session = Session()
    local_commands_c = create_commands_cache(local_commands)
    remote_commands_c = create_commands_cache(remote_commands)
    for local_command in local_commands:
        session.add(local_command)
        if local_command not in remote_commands_c:
            # local command not in remote, add it to be pushed
            push_session.append(local_command)
    for remote_command in remote_commands:
        if remote_command.created_ts not in local_commands_c:
            # remote command is not local, add it and mark as synced
            remote_command.synced = True
            session.add(remote_command)
    session_commit(session)
    # push local to remote srv
    batch_post_request(push_session)


def get_local_unsynced():
    session = Session()
    unsynced_history_commands = session.query(HistoryCommand).filter_by(synced=False).all()
    if unsynced_history_commands:
        print(">>> {} unsynced commands".format(len(unsynced_history_commands)))
    return unsynced_history_commands


def fetch_remote_unsynced():
    while True:
        session = Session()
        try:
            last_not_host = session.query(HistoryCommand).filter(
                HistoryCommand.host != HOST).order_by(
                HistoryCommand.created_dt.desc()).first()
            if last_not_host:
                since = last_not_host.created_dt
            else:
                since = 0
            print("  > Fetching unsynced...")
            response = requests.get(TRACKER_SESSIONS_API_URL.format(sensitive.TRACKER_ID),
                                    params={
                                        'api_key': sensitive.API_KEY,
                                        'since': since,
                                        'tracker_type': 'bash_history',
                                        'client_not': HOST
                                    })
            response_json = response.json()
            tracker_events = response_json.get('tracker_events')
            print("  > Unsynced: {}".format(len(tracker_events)))
            for tracker_event in tracker_events:
                tracker_event_meta = tracker_event.get('meta', {})
                history_command = HistoryCommand(command=tracker_event['key'],
                                                 created_dt=dt_parser.parse(tracker_event['time']),
                                                 host=tracker_event.get('client'),
                                                 synced=True)
                session.add(history_command)
            if tracker_events:
                session_commit(session)
                print("  > Created {} fetched elements!".format(len(tracker_events)))
        except Exception as e:
            print("  > Failed to fetch unsynced!")
            print(e)
            traceback.print_exc()
        time.sleep(FETCH_SLEEP)
        if poison_pill:
            return


def parse_push():
    for history_command in parse_history_file(iterable=follow(BASH_HISTORY_FILE)):
        post_request(history_command)


if __name__ == '__main__':
    if not os.path.exists(sqllite_path):
        # The local database doesn't exist, initialize by creating it
        create_db()
        # Get local commands from history file
        local_commands = get_local_all()
        # Fetch remote commands and then merge the two
        try:
            remote_commands = fetch_remote_all()
        except Exception as e:
            print("  > Failed to fetch remote!")
            print(e)
            traceback.print_exc()
            remote_commands = []
        merge_local_remote(local_commands, remote_commands)
    # In any case, push the local unsynced to the server
    unsynced_commands = get_local_unsynced()
    batch_post_request(unsynced_commands)
    # Start fetching remote commands in a thread
    fetch_thread = threading.Thread(target=fetch_remote_unsynced)
    try:
        fetch_thread.start()
        # Start monitoring the local history file
        parse_push()
    except KeyboardInterrupt:
        print("Caught Keyboard Interrupt, quitting...")
        poison_pill = 'die'
        fetch_thread.join()
    exit(0)
